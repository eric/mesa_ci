import sys
import os
try:
    from urllib2 import urlopen, quote
    from urlparse import urlparse as parse
except:
    from urllib import parse
    from urllib.request import urlopen, quote

url_components = parse(os.environ["BUILD_URL"])

import_url = ("http://" + url_components.netloc +
              "/job/ImportResults/buildWithParameters?"
              "token=noauth&"
              "url={0}".format(quote(os.environ["BUILD_URL"])))

print("triggering: " + import_url)
urlopen(import_url)

