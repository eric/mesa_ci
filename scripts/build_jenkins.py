#!/usr/bin/env python3
import argparse
import datetime
import glob
import hashlib
import os
import sys
import time
import xml.etree.cElementTree as et
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from dependency_graph import DependencyGraph
from jenkins import Jenkins
from options import Options
from project_map import ProjectMap
from repo_set import BuildSpecification, RevisionSpecification
from utils.command import run_batch_command, rmtree


def strip_passing_tests(root):
    # these statuses are interpreted as 'passing' CI expectations and will be
    # stripped
    strip_statuses = ['pass', 'skip', 'warn']
    for a_suite in root.findall("testsuite"):
        for a_test in a_suite.findall("testcase"):
            # Tests with status=fail may be skipped by the component, which is
            # why subelems are searched
            if ((a_test.attrib["status"] in strip_statuses and not
                    a_test.findall("failure")) or a_test.findall("skipped")):
                a_suite.remove(a_test)
                continue


def collate_tests(result_path, out_test_dir, strip_passes=False):
    src_test_dir = result_path + "/test"
    if strip_passes:
        print("Note: passing tests will be stripped from the result file(s)")
        os.makedirs(out_test_dir + "/test/stripped", exist_ok=True)
    print("collecting tests from " + src_test_dir)
    i = 0
    while i < 10 and not os.path.exists(src_test_dir):
        i += 1
        print("sleeping, waiting for test directory: " + src_test_dir)
        time.sleep(10)
    if not os.path.exists(src_test_dir):
        print("no test directory found: " + src_test_dir)
        return

    cmd = ["cp", "-a", "-n",
           src_test_dir,
           out_test_dir]
    run_batch_command(cmd)

    # Junit files must have a recent time stamp or else Jenkins will
    # not parse them.
    for a_file in glob.glob(out_test_dir + "/test/**/*.xml", recursive=True):
        os.utime(a_file, None)

        # Remove passing tests from xml and write to different directory so
        # full results are preserved
        if strip_passes:
            a_file_name = a_file.split('/')[-1]
            a_new_file_path = out_test_dir + "/test/stripped/" + a_file_name
            if os.path.isdir(a_file):
                continue
            try:
                xml = et.parse(a_file)
            except et.ParseError:
                continue
            strip_passing_tests(xml)
            xml.write(a_new_file_path, encoding='utf-8', xml_declaration=True)


def main():
    # reuse the options from the gasket
    o = Options([sys.argv[0]])
    description="builds a component on jenkins"
    parser= argparse.ArgumentParser(description=description,
                                    parents=[o._parser],
                                    conflict_handler="resolve")
    parser.add_argument('--project', dest='project', type=str, default="",
                        help='Project to build. Default project is specified '\
                        'for the branch in build_specification.xml')

    parser.add_argument('--branch', type=str, default="mesa_master",
                        help="Branch specification to build.  "\
                        "See build_specification.xml/branches")

    parser.add_argument('--rebuild', type=str, default="false",
                        choices=['true', 'false'],
                        help="specific set of revisions to build."
                        "(default: %(default)s)")
    parser.add_argument('--results_subdir', type=str, default="",
                        help="Subdirectory under results_dir to place results."
                        " Use this to prevent conflicts when running"
                        "multiple concurrent tests on the same branch.")
    parser.add_argument('--strip_passes', action='store_true',
                        help=("Strip passing tests from results. Results will "
                              "be placed under 'results/test/stripped'"))

    args = parser.parse_args()
    projects = []
    if args.project:
        projects = args.project.split(",")
    branch = args.branch
    rebuild = args.rebuild
    results_subdir = args.results_subdir or branch

    # some build_local params are not handled by the Options, which is
    # used by other modules.  This code strips out incompatible args
    o = Options(["bogus"])
    vdict = vars(args)
    del vdict["project"]
    del vdict["branch"]
    del vdict["rebuild"]
    o.__dict__.update(vdict)
    sys.argv = ["bogus"] + o.to_list()
    
    bspec = BuildSpecification()
    pm = ProjectMap()
    rmtree(pm.source_root() + "/test_summary.txt")

    branchspec = bspec.branch_specification(branch)
    if not projects:
        projects = [branchspec.project]
    depGraph = DependencyGraph(projects, o)
    revspec = RevisionSpecification(only_projects=depGraph.all_sources())
    priority = branchspec.priority
    # priority from env should take precedence
    if 'BUILD_PRIORITY' in os.environ:
        try:
            priority = int(os.environ.get('BUILD_PRIORITY'))
        except TypeError:
            pass

    print("Building revision: " + revspec.to_cmd_line_param())
    # create a result_path that is unique for this set of builds
    spec_xml = pm.build_spec()
    # perf CI results path should be results/mesa=<sha> and not a hash of
    # components.
    hostname = spec_xml.find("build_master").attrib.get('hostname')
    if hostname and hostname == 'otc-mesa-android':
        result_name = 'mesa=' + revspec.revision('mesa')
    else:
        result_name = hashlib.md5(revspec.to_cmd_line_param().encode('utf-8')).hexdigest()

    results_dir = spec_xml.find("build_master").attrib["results_dir"]
    result_path = "/".join([results_dir, results_subdir, result_name,
                            o.type])
    print("Result path: {}".format(result_path))
    o.result_path = result_path
    # recreate the dependency graph with an options object that specifies the result path
    depGraph = DependencyGraph(projects, o)

    if rebuild == "true" and os.path.exists(result_path):
        print("Removing existing results.")
        mvdir = os.path.normpath(result_path + "/../" + datetime.datetime.now().isoformat())
        os.rename(result_path, mvdir)

    # use a global, so signal handler can abort builds when scheduler
    # is interrupted
    global jen

    jen = Jenkins(result_path=result_path,
                     revspec=revspec)

    out_test_dir = pm.output_dir()
    if os.path.exists(out_test_dir):
        rmtree(out_test_dir)
    os.makedirs(out_test_dir)

    # to collate all logs in the scheduler
    out_log_dir = pm.output_dir()
    if os.path.exists(out_log_dir):
        rmtree(out_log_dir)
    os.makedirs(out_log_dir)

    # Add a revisions.xml file
    if not os.path.exists(result_path):
        os.makedirs(result_path)
    revspec.to_elementtree().write(os.path.join(result_path, 'revisions.xml'))

    # use a global, so signal handler can abort builds when scheduler
    # is interrupted
    try:
        jen.build_all(depGraph, branch=branch, priority=priority)
    finally:
        collate_tests(result_path, out_test_dir,
                      strip_passes=args.strip_passes)

if __name__=="__main__":
    try:
        main()
    except SystemExit:
        # Uncomment to determine which version of argparse is throwing
        # us under the bus.

        #  Word of Wisdom: Don't call sys.exit
        #import traceback
        #for x in traceback.format_exception(*sys.exc_info()):
        #    print(x)
        raise

# vim: ft=python
