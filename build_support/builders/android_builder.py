# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import configparser
import glob
import os
import subprocess
import sys
from utils.utils import cpu_count
from utils.command import run_batch_command, rmtree
from utils.utils import get_conf_file, NoConfigFile
from dependency_graph import DependencyGraph
from export import Export
from options import Options
from project_map import ProjectMap
from repo_set import RepoSet


class AndroidBuilder(object):
    def __init__(self, src_location, modules):
        self._options = Options()
        self._project_map = ProjectMap()
        self.project = self._project_map.current_project()
        self._src_dir = os.path.join(self._project_map._source_root, "repos",
                                     "android")
        self._android_orig_src = src_location
        self._android_target = "celadon_tablet-userdebug"
        self._env = {
           'ANDROID_TARGET': self._android_target,
           'ANDROID_SOURCE': self._src_dir,
           'ANDROID_MODULE': " ".join(modules),
           'NUM_CPUS': str(cpu_count()),
        }
        self._build_helper = os.path.join(self._project_map.source_root(),
                                          "repos/mesa_ci/build_support",
                                          "android_builder.sh")

        # Location of mesa source to build
        self._mesa_src = os.path.join(self._project_map._source_root, "repos",
                                      "mesa")
        if not os.path.exists(self._mesa_src):
            assert os.path.exists(self._mesa_src), ("ERROR: Mesa source directory "
                                                    "not found: {}".format(self._mesa_src))

    def build(self):
        # Copy android source tree to repos
        if not os.path.exists(self._src_dir):
            print("Copying android source from: {}".format(self._android_orig_src))
            run_batch_command(["cp", "-al",
                               os.path.expanduser(self._android_orig_src),
                               self._src_dir])
        # Local location of mesa source in Android tree
        self._mesa_local_src = os.path.join(self._project_map._source_root,
                                            "repos/android/vendor/intel",
                                            "external/project-celadon/mesa")
        # Copy mesa source to subdir in android tree
        if os.path.exists(self._mesa_local_src):
            if os.path.islink(self._mesa_local_src):
                os.remove(self._mesa_local_src)
            else:
                rmtree(self._mesa_local_src)
        # Remove any other mesa dirs in the tree
        if os.path.exists(os.path.join(self._src_dir,
                                       'hardware/intel/external/mesa3d-intel')):
            rmtree(os.path.join(self._src_dir,
                                'hardware/intel/external/mesa3d-intel'))

        run_batch_command(["cp", "-al", self._mesa_src, self._mesa_local_src])
        # Remove patch(es) from the celadon project that do not apply to
        # mesa master, since 'lunch' treats patch conflicts as fatal
        bad_patches = ['vendor/intel/utils/bsp_diff/common/hardware/intel/external/mesa3d-intel/672265_2-WA-for-random-vulkan-link-error-on-Android-Q.patch',
                       'vendor/intel/utils/aosp_diff/celadon_tablet/packages/services/Car/672723_2-APL-NUC-remove-CPMS.patch',]
        for patch in bad_patches:
            patch_path = os.path.join(self._src_dir, patch)
            if os.path.exists(os.path.join(patch_path)):
                print("Removing bad patch from celadon: {}".format(patch_path))
                os.remove(patch_path)
        # apply patches if they exist
        for patch in sorted(glob.glob(os.path.join(
                            self._project_map.project_build_dir(),
                            "*.patch"))):
            os.chdir(self._mesa_local_src)
            try:
                run_batch_command(["git", "am", patch])
            except subprocess.CalledProcessError:
                print("WARN: failed to apply patch: {}".format(patch))
                run_batch_command(["git", "am", "--abort"])

        print("Enabling Iris...")
        target_base = self._android_target.replace("-eng", "")
        target_base = target_base.replace("-userdebug", "")
        brd_config = os.path.join(self._src_dir,
                                  "device/intel/project-celadon",
                                  target_base,
                                  "BoardConfig.mk")
        run_batch_command(["sed", "-i", "s/i965/i965 iris/", brd_config])
        build_fail = False
        try:
            run_batch_command([self._build_helper, 'build'], env=self._env)
        except subprocess.CalledProcessError:
            build_fail = True
        expected_to_fail = self._expected_to_fail()
        if build_fail and not expected_to_fail:
            Export().create_failing_test("Android Build Test",
                                         ("ERROR: Failed to build Mesa. "
                                          "See the console log for the "
                                          "android-buildtest component "
                                          "for more details."))
        elif not build_fail and expected_to_fail:
            Export().create_failing_test("Android Build Test",
                                         ("this build succeeded when it "
                                          "expected to fail"))

    def clean(self):
        if os.path.exists(self._src_dir):
            rmtree(self._src_dir)

    def test(self):
        pass

    def _expected_to_fail(self):
        """ parses config file for project and determines if project is
        expected to pass or fail. 'expected-failures' and 'expected-crashes'
        sections are treated the same. """

        try:
            conf_file = get_conf_file(self._options.hardware,
                                      self._options.arch,
                                      project=self.project)
        except NoConfigFile:
            print("WARN: no config file found for " + self.project)
            return False

        c = configparser.SafeConfigParser(allow_no_value=True)
        c.read(conf_file)
        commit = None
        expect_fail = False
        try:
            commit = c.get('expected-failures', self.project)
            expect_fail = True
        except configparser.NoOptionError:
            try:
                commit = c.get('expected-crashes', self.project)
                expect_fail = True
            except configparser.NoOptionError:
                pass
        deps = DependencyGraph(self.project,
                               Options(args=[sys.argv[0]]),
                               repo_set=None)
        long_revisions = RepoSet().branch_missing_revisions(deps)
        missing_revisions = [a_rev[:6] for a_rev in long_revisions]
        for missing_commit in missing_revisions:
            if commit and missing_commit in commit:
                print("Buildtest status changed by a commit not in this "
                      "build.")
                expect_fail = False
        return expect_fail
