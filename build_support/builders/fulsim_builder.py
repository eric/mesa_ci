# Copyright (C) Intel Corp.  2019.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import os
import subprocess
import sys
import glob
import urllib
import zipfile
import tempfile
from utils.command import rmtree, run_batch_command
from export import Export
from options import Options
from project_map import ProjectMap
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None

# Note: values are urls templates, {0} will be replaced with the fulsim build
# number
fulsim_urls = {}
if internal_vars:
    fulsim_urls.update(internal_vars.fulsim_urls)

FULSIM_CACHE_CAPACITY = 100
FULSIM_CACHE_EVICT_AMOUNT = 1
FULSIM_CACHE_PATH = "/mnt/fulsim_cache"
FULSIM_CACHE_MAX_RETRIES = 3

class FulsimBuilder(object):
    def __init__(self, buildnum):
        self._options = Options()
        self._project_map = ProjectMap()
        self._env = {}
        self.hardware = self._options.hardware.replace('_iris', '')
        self.buildnum = buildnum
        # Install path is /tmp/build_root/opt/fulsim/<hardware>
        self.fulsim_install_path = (self._project_map.build_root()
                                    + '/opt/fulsim/' + self.hardware)
        if self.hardware not in fulsim_urls:
            assert False, ("ERROR: No fulsim source url defined for hardware"
                           ": {}".format(self.hardware))

    def build(self):
        fulsim_archive_path = os.path.join("/tmp", "fulsim_" + self.hardware
                                           + "_" + self.buildnum + ".zip")

        FulsimCache(self.hardware, self.buildnum).get(fulsim_archive_path)

        if not os.path.exists(self.fulsim_install_path):
            os.makedirs(self.fulsim_install_path)

        print("Extracting fulsim archive {}".format(fulsim_archive_path))

        try:
            run_batch_command(["unzip", "-q", fulsim_archive_path, "-d",
                               self.fulsim_install_path])
        except subprocess.CalledProcessError:
            if os.path.exists(self.fulsim_install_path):
                rmtree(self.fulsim_install_path)
            assert False, ("ERROR: The fulsim archive is invalid and as been "
                           "removed. This could be caused by a corrupted "
                           "download or if the requested fulsim version is "
                           "no longer available.")
        finally:
            os.remove(fulsim_archive_path)

        unneeded_dirs = self._project_map.project_build_dir() + "/unneeded_dirs"

        if os.path.exists(unneeded_dirs):
            with open(unneeded_dirs, "r") as unneeded_dirs:
                for d in unneeded_dirs.readlines():
                    d_stripped = d.rstrip()
                    if not d_stripped or d_stripped.startswith("#"):
                        continue
                    dir_path = self.fulsim_install_path + "/" + d_stripped
                    rmtree(dir_path)

        Export().export()

    def clean(self):
        if os.path.exists(self.fulsim_install_path):
            rmtree(self.fulsim_install_path)

    def test(self):
        pass


class FulsimCache(object):
    def __init__(self, hardware, buildnum):
        self._hardware = hardware
        self._buildnum = buildnum
        self._key = self._cache_key()

    def get(self, download_path="/tmp", retries=FULSIM_CACHE_MAX_RETRIES):
        if retries < 0:
            raise FileNotFoundError("FATAL ERROR: fulsim archive couldn't be downloaded")

        if retries > FULSIM_CACHE_MAX_RETRIES:
            retries = FULSIM_CACHE_MAX_RETRIES

        try:
            if not os.path.exists(FULSIM_CACHE_PATH):
                self._get_remote_archive(download_path)
                return

            if not os.path.exists(self._key):
                self._cache_miss()

            run_batch_command(["touch", "-c", self._key])
            run_batch_command(["rsync", self._key, download_path])
        except (subprocess.CalledProcessError, FileNotFoundError):
            print("WARN: Failed to copy fulsim archive. Retrying...")
            self.get(download_path, retries - 1)

        print("fulsim archive downloaded from cache successfully")

    def _cache_miss(self): #SOMEWHAT thread safe; unlikely, but this method might delete more archives than expected
        print("WARN: fulsim cache miss")
        cache_len = len(os.listdir(FULSIM_CACHE_PATH))

        if cache_len >= FULSIM_CACHE_CAPACITY:
            print("WARN: fulsim cache full, removing old archives")
            rm_files = glob.glob(os.path.join(FULSIM_CACHE_PATH, "*"))

            rm_files.sort(key=os.path.getmtime)
            for f in rm_files[0:FULSIM_CACHE_EVICT_AMOUNT]:
                try:
                    tmp = ""
                    if f.startswith(os.path.join(FULSIM_CACHE_PATH, "fulsim_")) and f.endswith(".zip"):
                        (_, tmp) = tempfile.mkstemp(dir=FULSIM_CACHE_PATH)
                        os.rename(f, tmp) #atomic per POSIX standard
                        os.remove(tmp)
                except (FileNotFoundError, IsADirectoryError):
                    pass
                finally:
                    if os.path.isfile(tmp):
                        os.remove(tmp)

        print("INFO: attempting to add archive to fulsim cache")
        self._get_remote_archive(self._key)
        print("INFO: " + os.path.basename(self._key) + " added to fulsim cache")

    def _get_remote_archive(self, download_path):
        if os.path.isdir(download_path):
            file_path = os.path.join(download_path, os.path.basename(self._key))
        else:
            file_path = download_path

        for url in fulsim_urls[self._hardware]: #may attempt to speed up later
            try:
                tmp = ""
                print("Attempting to download fulsim from: "
                      "{}".format(url.format(self._buildnum)))
                (_, tmp) = tempfile.mkstemp(dir=os.path.dirname(file_path))
                urllib.request.urlretrieve(url.format(self._buildnum), tmp)
                os.rename(tmp, file_path) #atomic per POSIX standard
                break
            except urllib.error.HTTPError:
                print("The requested fulsim version was not found at the given "
                      "url.")
            finally:
                if os.path.isfile(tmp):
                    os.remove(tmp)

        if os.path.exists(file_path):
            print("fulsim archive copied to " + file_path)
        else:
            raise FileNotFoundError("ERROR: Unable to retrieve the requested fulsim "
                                    "version")

        return file_path

    def _cache_key(self):
        return os.path.join(FULSIM_CACHE_PATH, "fulsim_" + self._hardware
                            + "_" + self._buildnum + ".zip")
