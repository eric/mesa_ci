#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2020.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import argparse
import git
import hashlib
import json
import logging
import os
import signal
import sys
import time
from collections import namedtuple
from datetime import datetime, timedelta
from urllib.request import urlopen, URLError, HTTPError
from pony.orm import db_session
from pony import orm

sys.path.append("/var/cache/mesa_jenkins/repos/mesa_ci")
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "../..", "build_support"))
sys.path.append("/var/cache/mesa_jenkins/repos/mesa_perf_results")

from models import db
from project_map import ProjectMap
from repo_set import BuildSpecification
from utils.utils import write_pid, signal_handler_quit

BuildShard = namedtuple('BuildShard', 'mesa_sha platform benchmark hash')

# Interval for building commits on the given (usually 'master') branch
BRANCH_COMMIT_INTERVAL = 180

# relatively recent branch
BRANCH_COMMIT = "19.3-branchpoint"

# Initialize db object
sql_user = 'jenkins'
if "SQL_DATABASE_USER" in os.environ:
    sql_user = os.environ["SQL_DATABASE_USER"]

sql_pw = ''
if "SQL_DATABASE_PW_FILE" in os.environ:
    sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
    if os.path.exists(sql_pw_file):
        with open(sql_pw_file, 'r') as f:
            sql_pw = f.read().rstrip()
sql_host = "localhost"
if "SQL_DATABASE_HOST" in os.environ:
    sql_host = os.environ["SQL_DATABASE_HOST"]

if sql_pw:
    db.bind('mysql', host=sql_host, user=sql_user, password=sql_pw,
            database='mesa_perf_results')
else:
    db.bind('mysql', host=sql_host, user=sql_user,
            database='mesa_perf_results')
db.generate_mapping(create_tables=True)

log_format = "%(funcName)s():%(lineno)i: %(levelname)s %(message)s"
logging.basicConfig(level=logging.INFO, format=log_format)
log = logging.getLogger()


class Jenkins():

    def __init__(self, host, job):
        self._host = host
        self._job = job

    def get_job_status(self):
        url = (f"http://{self._host}/api/json?tree=jobs[name,lastBuild[building]]")
        try:
            resp = urlopen(url).read()
        except (URLError, HTTPError):
            log.error(f"Unable to open url: {url}")
            return
        tree = json.loads(resp)['jobs']
        jobs = {}
        for item in tree:
            if not item.get('lastBuild'):
                continue
            try:
                jobs[item['name']] = item['lastBuild']['building']
            except KeyError:
                log.error(f"Cannot parse job data from: {item}")
                continue

        return jobs

    def is_job_busy(self, job=None):
        if not job:
            job = self._job
        job_status = self.get_job_status()
        for job, status in job_status.items():
            if job.lower() == 'leeroy':
                continue
            if status:
                return True
        return False

    def trigger_build(self, sha):
        """ Trigger build with given sha, return the url of the new build or
        None if error"""

        url = (f"http://{self._host}/job/{self._job}/buildWithParameters/"
               f"?token=noauth&revision=mesa={sha}")
        log.info(f"Triggering build for sha {sha}: {url}")
        resp = urlopen(url)
        queue_item_url = resp.headers.get('Location')
        if not queue_item_url:
            log.error("Did not receive queue item for triggered build")
            return None
        queue_item_url += '/api/json'
        timedout = False
        # longer timeout since executors on the target system (e.g. CI master)
        # might be full and the build might have to wait in the queue for a bit
        timeout = datetime.now() + timedelta(minutes=10)
        build_url = None
        # poll queue item for build url, which becomes available once the
        # queued item is turned into an executing build
        while not build_url and not timedout:
            timedout = timeout < datetime.now()
            try:
                resp = urlopen(queue_item_url).read()
            except (URLError, HTTPError):
                time.sleep(1)
                continue
            tree = json.loads(resp)
            executable = tree.get('executable')
            if not executable:
                log.info("Build still in queue, retrying...")
                time.sleep(1)
                continue
            build_url = executable.get('url')
        return build_url

    def _is_build_running(self, url):
        url += '/api/json?tree=result'
        try:
            resp = urlopen(url).read()
        except (URLError, HTTPError):
            log.error(f"Unable to open url: {url}")
            return
        tree = json.loads(resp)
        result = tree.get('result')
        # if a result is present, the build is not running
        return not result

    def wait_for_build(self, build_url, timeout=120):
        """
        blocks until the the given build is finished or until the given
        timeout (in minutes) is reached, returns True if timed out waiting for
        build
        """
        timedout = False
        timeout = datetime.now() + timedelta(minutes=timeout)
        while not timedout and self._is_build_running(build_url):
            log.info(f"Build still running: {build_url}")
            time.sleep(120)
            timedout = timeout < datetime.now()
        if timedout:
            log.error("Timed out waiting for build to complete...")
        return timedout


class FakeJenkins(Jenkins):
    """" A stub Jenkins() that does not trigger builds (useful for
    'simulating') """

    def __init__(self, host, job):
        super().__init__(host, job)

    def trigger_build(self, sha):
        log.info("fake 'trigger_build'")

    def wait_for_build(self, build_url, timeout=120):
        log.info("fake 'wait_for_build'")


def get_bs_hardware(build_spec, project):
    """
    Find all hardware for all prereqs in the project recursively.
    Returns a dict of {prereq: [hardware list]}
    """
    bs_benchmarks = {}
    for prereq in project.findall('prerequisite'):
        if prereq.get('hardware'):
            bs_benchmarks[prereq.attrib['name']] = set(prereq.attrib['hardware'].split(','))
        else:
            for p in build_spec.find('projects').findall('project'):
                if prereq.attrib['name'] == p.attrib['name']:
                    bs_benchmarks.update(get_bs_hardware(build_spec, p))
                    break
    return bs_benchmarks


@db_session
def score_has_mesa_sha(score, sha):
    for rev in score.build.revisions:
        if rev.project == 'mesa' and rev.sha.startswith(sha):
            return True
    return False


def get_shard_hash(mesa_sha, platform, benchmark):
    hash = hashlib.sha1()
    project = ''
    if benchmark:
        # benchmark is None if the shard references a benchmark not in the db
        # yet
        project = benchmark.project
    shard_str = ''.join([mesa_sha, platform.gen_driver, project])
    hash.update(shard_str.encode())
    return hash.hexdigest()


@db_session(strict=True)
def get_missing_shards(bs_benchmarks, job_name, build_sha=None):
    """
    Return a dict of BuildShards from the db that are missing results/scores
    for any benchmarks/platforms. Key is a hash of shard features generated
    with get_shard_hash().
    """
    job = orm.select(j for j in db.Job if j.name == job_name).first()
    # no job builds in db
    if not job:
        log.info(f"No builds found for job: {job}")
        return {}

    if build_sha:
        job_shas = [build_sha]
    else:
        job_shas = orm.select(r.sha for r in db.Revision
                              if (r.project == 'mesa'
                                  and r.job.name == job.name))[:]
    # for each sha, make sure every benchmark has results for all platforms
    missing_shards = {}
    bs_benchmark_names = set(bs_benchmarks.keys())
    for sha in job_shas:

        # find builds missing shards for specific benchmarks
        build_benchmarks = set(
            orm.select(s.benchmark.project for b in db.Build
                       if b.job.name == job.name
                       for s in db.Score
                       if s.build.build_id == sha)[:])
        for missing_benchmark in (bs_benchmark_names - build_benchmarks):
            # Note: this will include benchmarks defined in the build_spec
            # which might not be in the results db yet.
            platform = None
            for hardware in bs_benchmarks[missing_benchmark]:
                platform = orm.select(p for p in db.Platform
                                      if p.gen_driver == hardware).first()
                benchmark = orm.select(b for b in db.Benchmark
                                       if b.project == missing_benchmark).first()
                shard_hash = get_shard_hash(sha, platform, benchmark)
                build_shard = BuildShard(mesa_sha=sha, platform=platform,
                                         benchmark=benchmark, hash=shard_hash)
                if sha in missing_shards:
                    if build_shard in missing_shards[sha]:
                        # don't add duplicates
                        continue
                    missing_shards[sha].append(build_shard)
                else:
                    missing_shards[sha] = [build_shard]
        # find builds missing shards for specific hardware
        for project in bs_benchmarks:
            score_platforms = set(orm.select(s.platform for s in db.Score
                                             if s.job.name == job.name
                                             and s.build.build_id == sha
                                             and s.benchmark.project == project)[:])
            score_hw = set([p.gen_driver for p in score_platforms])

            for missing_hardware in (bs_benchmarks[project] - score_hw):
                for hardware in bs_benchmarks[project]:
                    platform = orm.select(p for p in db.Platform
                                          if p.gen_driver == hardware).first()
                    benchmark = orm.select(b for b in db.Benchmark
                                           if b.project == project).first()
                    shard_hash = get_shard_hash(sha, platform, benchmark)
                    build_shard = BuildShard(mesa_sha=sha, platform=platform,
                                             benchmark=benchmark,
                                             hash=shard_hash)
                    if sha in missing_shards:
                        if build_shard in missing_shards[sha]:
                            # don't add duplicates
                            continue
                        missing_shards[sha].append(build_shard)
                    else:
                        missing_shards[sha] = [build_shard]
    missing_shards = remove_blocked_shards(missing_shards)
    for sha, shards in missing_shards.items():
        for shard in shards:
            msg = (f"*missing shard* mesa: {sha}, "
                   f"platform: {shard.platform.gen_driver}")
            if shard.benchmark:
                msg += f", benchmark: {shard.benchmark.project}"
            else:
                msg += ", all benchmarks"
            log.info(msg)
    return missing_shards


def get_missing_builds(job):
    shas_at_interval = set()
    # sets are unordered, so this results in a 'randomization' of shas
    missing_shas = set()
    bspec = BuildSpecification()
    # call checkout with a fake branch since we're only interested in
    # fetching/checking out mesa and no other supporting projects
    tries = 1
    while tries <= 10:
        try:
            bspec.checkout('fake_branch', ['mesa=origin/main'])
            break
        except git.exc.GitCommandError as e:
            tries += 1
            log.error(f"Unable to checkout repo, retry {tries}/10: {e}")
            time.sleep(5)
    # Note: the retry is only for when the repo fetch/checkout conflicts with
    # the poll_branches service, it seems highly unlikely it would get to this
    # point and _still_ not have successfully fetched/checked out, so nothing
    # is done here.

    mesa_repo = git.Repo(ProjectMap().project_source_dir("mesa"))

    branch_commit = mesa_repo.tags[BRANCH_COMMIT].commit.hexsha

    log.info("Selecting Mesa commits at interval: "
             f"{str(BRANCH_COMMIT_INTERVAL)}")

    total_commits = mesa_repo.git.rev_list(["HEAD", "--count"])
    commits_to_branch = mesa_repo.git.rev_list([branch_commit, "--count"])
    try:
        commits_count = int(total_commits) - int(commits_to_branch)
    except TypeError as e:
        log.info(f"Unable to get number of commits: {e}")
        return missing_shas

    commits = mesa_repo.git.rev_list(['--abbrev-commit', '--topo-order',
                                      branch_commit + '..HEAD']).split()
    for i in range(commits_count, 0, -BRANCH_COMMIT_INTERVAL):
        shas_at_interval.add(commits[i - 1])

    log.info("Finding missing builds")
    for sha in shas_at_interval:
        if not orm.select(b for b in db.Build if b.build_id == sha
                          and b.job.name == job).first():
            missing_shas.add(sha)

    # remove any builds that are mentioned in the Blocklist db table. These
    # would have been added if an attempt to run the build crashed and resulted
    # in no scores imported into the db
    bl_build_shas = set(orm.select(b.build_id for b in db.Blocklist)[:])
    for sha in bl_build_shas:
        if sha in missing_shas:
            log.info(f"*Ignoring* mesa={sha} because it is in the db "
                     "blocklist")
            missing_shas.remove(sha)

    return missing_shas


@db_session
def remove_blocked_shards(shards_dict):
    filtered_shards_dict = {}
    # remove shards that are blocked from running

    for mesa_sha, shards in shards_dict.items():
        for shard in shards:
            shard_id = get_shard_hash(shard.mesa_sha, shard.platform,
                                      shard.benchmark)
            bl_shard = orm.select(b for b in db.Blocklist
                                  if b.id == shard_id).first()
            if bl_shard:
                msg = ("*ignoring* shard from blocklist: mesa: " +
                       f"{shard.mesa_sha}, platform: "
                       f"{shard.platform.gen_driver}")
                if shard.benchmark:
                    msg += ", benchmark: " + shard.benchmark.name
                log.info(msg)
                continue
            if mesa_sha in filtered_shards_dict:
                filtered_shards_dict[mesa_sha].append(shard)
            else:
                filtered_shards_dict[mesa_sha] = [shard]
    return filtered_shards_dict


@db_session
def get_build_date(job, build_id):
    # commit so that db_session_cache is cleared...
    db.commit()
    build_date = orm.select(b.build_date for b in db.Build
                            if b.build_id == build_id
                            and b.job.name == job).first()
    return build_date


@db_session
def process_missing_shards(job, simulate=False):
    """
    Find all missing shards for the given job, and trigger builds to get
    results for them. This blocks until all missing shards either have results
    or until they are added to the blocklist because they failed to generate
    results after triggering.
    """

    project_map = ProjectMap()
    bs = project_map.build_spec()
    # get mapping of benchmark --> [hardware list] in current build spec
    bs_benchmarks = {}
    for project in bs.find('projects').findall('project'):
        if project.get('name') == 'perf-all':
            bs_benchmarks.update(get_bs_hardware(bs, project))
            break
    if simulate:
        jenkins = FakeJenkins(bs.find("build_master").attrib["host"], job)
    else:
        jenkins = Jenkins(bs.find("build_master").attrib["host"], job)
    # process missing shards first, since resolving incomplete builds is
    # more important than adding new builds
    while True:
        log.info("Getting all missing shards")
        missing_shards = get_missing_shards(bs_benchmarks, job)

        if not missing_shards:
            log.info("No more missing shards")
            break
        build_sha, build_shards = missing_shards.popitem()
        old_build_date = get_build_date(job, build_sha)
        # wait for jenkins job idle
        while jenkins.is_job_busy(job):
            time.sleep(10)
            log.info("Waiting 10s for jenkins to finish existing build "
                     f"for: {job}")
        # build sha
        log.info(f"Running build for mesa {build_sha} because the "
                 "following shard(s) are missing:")
        for shard in build_shards:
            gen_driver = shard.platform.gen_driver
            benchmark = "all benchmarks"
            if shard.benchmark:
                benchmark = shard.benchmark.name
            log.info(f"    {gen_driver}: {benchmark}")
        build_url = jenkins.trigger_build(build_sha)
        # wait for sha
        log.info("Waiting for jenkins build to complete...")
        jenkins.wait_for_build(build_url)
        log.info("Jenkins build completed!")

        # wait for build in db to be updated
        # if a build crashes or fails in some way that prevents new results
        # from being exported to the db, then it will time out. This isn't
        # a big deal since the timeout will be reached, shards will still
        # be missing from the build, and they'll be added to the blocklist
        # TODO: check build result from jenkins to try and determine if
        # results will come, which is faster than waiting for the result
        # timeout
        timedout = False if not simulate else True
        timeout = datetime.now() + timedelta(minutes=5)
        log.info("Looking for new results")
        while not timedout:
            new_build_date = get_build_date(job, build_sha)
            if new_build_date > old_build_date:
                break
            time.sleep(10)
            timedout = timeout < datetime.now()
        if timedout:
            log.error("Unable to find new results for build using mesa "
                      f"{build_sha}")
            log.error("This build and any incomplete shards will be added to "
                      "the blocklist.")

        # add any present shards from build_sha to blocklist
        # these shards were missing, and failed again, so CI should stop
        # trying to run them run
        log.info("Getting missing shards from completed build")
        new_missing_shards = get_missing_shards(bs_benchmarks, job, build_sha)
        if build_sha in new_missing_shards:
            for bad_shard in new_missing_shards[build_sha]:
                bad_shard_id = get_shard_hash(build_sha,
                                              bad_shard.platform,
                                              bad_shard.benchmark)
                db.Blocklist(id=bad_shard_id, build_id=build_sha,
                             platform=bad_shard.platform,
                             benchmark=bad_shard.benchmark)
                msg = ("Adding new shard to blocklist: "
                       f"{bad_shard.mesa_sha}, platform: "
                       f"{bad_shard.platform.gen_driver}")
                if bad_shard.benchmark:
                    msg += f", benchmark: {bad_shard.benchmark.name}"
                log.info(msg)
                # commit so blocklist in db is updated for subsequent calls
                # to get_missing_shards
                db.commit()


@db_session
def process_missing_builds(job, simulate=False):
    project_map = ProjectMap()
    missing_builds = set()
    # process missing mesa builds
    project_map = ProjectMap()
    bs = project_map.build_spec()
    # get mapping of benchmark --> [hardware list] in current build spec
    bs_benchmarks = {}
    for project in bs.find('projects').findall('project'):
        if project.get('name') == 'perf-all':
            bs_benchmarks.update(get_bs_hardware(bs, project))
            break
    if simulate:
        jenkins = FakeJenkins(bs.find("build_master").attrib["host"], job)
    else:
        jenkins = Jenkins(bs.find("build_master").attrib["host"], job)
    log.info("Getting missing build SHAs")

    missing_builds = get_missing_builds(job)
    if missing_builds:
        log.info("Missing builds: \n    " + "\n    ".join(missing_builds))
    else:
        log.info("No missing builds found")
    for build_sha in missing_builds:
        # wait for jenkins job idle
        while jenkins.is_job_busy(job):
            time.sleep(10)
            log.info("Waiting 10s for jenkins to finish existing build "
                     f"for: {job}")
        build_url = jenkins.trigger_build(build_sha)
        log.info("Waiting for jenkins build to complete...")
        jenkins.wait_for_build(build_url)
        log.info("Jenkins build completed!")

        # wait for build to show up in the db
        # if a build crashes or fails in some way that prevents new results
        # from being exported to the db, then it will time out. This isn't
        # a big deal since the timeout will be reached, shards will still
        # be missing from the build, and they'll be added to the blocklist
        # TODO: check build result from jenkins to try and determine if
        # results will come, which is faster than waiting for the result
        # timeout
        timedout = False if not simulate else True
        timeout = datetime.now() + timedelta(minutes=5)
        log.info("Looking for new results")
        while not timedout:
            if orm.select(b for b in db.Build if b.build_id == build_sha):
                # build exists in db now
                break
            time.sleep(10)
            timedout = timeout < datetime.now()
        if timedout:
            log.error("Unable to find new results for build using mesa "
                      f"{build_sha}")
            log.error("This build and any incomplete shards will be added to "
                      "the blocklist.")
        new_missing_builds = get_missing_builds(job)
        if build_sha in new_missing_builds:
            log.info(f"Build (mesa={build_sha}) is still missing all results, "
                     "blocking all shards from running again...")
            # add every platform to the blocklist for this mesa sha
            platforms = orm.select(p for p in db.Platform)[:]
            for platform in platforms:
                bad_shard_id = get_shard_hash(build_sha, platform, None)
                db.Blocklist(id=bad_shard_id, build_id=build_sha,
                             platform=platform, benchmark=None)
            db.commit()


@db_session
def main():

    # Write the PID file
    write_pid('/var/run/mesa_perf_history.pid')

    signal.signal(signal.SIGINT, signal_handler_quit)
    signal.signal(signal.SIGTERM, signal_handler_quit)

    description = ("Service for filling out historical mesa perf trend lines,"
                   "and filling in any missing results due to adding"
                   "benchmarks/games/platforms")
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-s', '--simulate', action="store_true",
                        help=("Do not trigger jenkins (useful "
                              "for local testing/debug"))
    parser.add_argument('-j', '--job', default="mesa_master",
                        help=("Job to use for searching results and "
                              "triggering builds (default: %(default)s"))
    args = parser.parse_args()

    if args.simulate:
        log.info("***Simulating***")

    # main application loop
    while True:
        # workaround for clearing orm cache...
        db.commit()
        process_missing_shards(args.job, simulate=args.simulate)

        process_missing_builds(args.job, simulate=args.simulate)

        time.sleep(30)


if __name__ == "__main__":
    main()
