#!/usr/bin/env python3

from __future__ import print_function
import os
import signal
import sys
import time
from datetime import datetime

sys.path.append("/var/cache/mesa_jenkins/repos/mesa_ci")
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "../..", "build_support"))
from project_map import ProjectMap
from repo_set import RepoSet, RepoNotCloned
from utils.utils import write_pid, signal_handler, signal_handler_quit


sys.argv[0] = os.path.abspath(sys.argv[0])


def main():
    # Write the PID file
    write_pid('/run/fetch_mirrors.pid')

    signal.signal(signal.SIGALRM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler_quit)
    signal.signal(signal.SIGTERM, signal_handler_quit)

    # running a service through intel's proxy requires some
    # annoying settings.
    os.environ["GIT_PYTHON_GIT_EXECUTABLE"] = "/usr/local/bin/git"
    # without this, git-remote-https spins at 100%
    os.environ["http_proxy"] = "http://proxy.jf.intel.com:911/"
    os.environ["https_proxy"] = "http://proxy.jf.intel.com:911/"
    cache_location = os.environ.get("FETCH_MIRRORS_CACHE_DIR")
    if not cache_location:
        cache_location = "/var/lib/git/"

    try:
        ProjectMap()
    except:
        sys.argv[0] = "/var/cache/mesa_jenkins/foo.py"

    if not os.path.exists(cache_location):
        os.makedirs(cache_location)
    # This *is* the service that creates the git cache, so do not use a cache
    # and create repos that are git clone mirrors:
    repos = RepoSet(repos_root=cache_location, use_cache=False, mirror=True)
    repos.clone()

    # used to determine if the repos should be pruned. 720 is about once a day,
    # assuming ~2min for each iteration of the loop
    gc_limit = 720
    fetch_without_gc = 0

    while True:
        try:
            timeout = 300  # 5 minutes
            needs_gc = False
            if fetch_without_gc >= gc_limit:
                needs_gc = True
                fetch_without_gc = 0
                timeout = 600  # 10 minutes
            signal.alarm(timeout)
            repos.fetch(gc=needs_gc)
        except RepoNotCloned:
            repos.clone()
        finally:
            signal.alarm(0)
            # pause a bit before fetching the next round
        fetch_without_gc += 1
        time.sleep(20)


if __name__ == "__main__":
    main()
